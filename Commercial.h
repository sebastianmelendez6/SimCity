#pragma once
#include "htable.h"
#include <vector>
#include <array>

using namespace std;

class cellArray {
public:
	cellArray(const int& row, const int& col, const Cell& cell, const array<Cell, 8>& adjacentCells)
		: row(row), col(col), cell(cell), adjacentCells(adjacentCells) {};
	array<Cell, 8> adjacentCells;
	int row;
	int col;
	Cell cell;
};


int simulateCommercial(int avail_workers, int avail_goods, HTable* table);