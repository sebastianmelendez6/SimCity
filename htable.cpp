#include "htable.h"

// constructor
HTable::HTable(int x) {
    count = x;
    htable = new list<Cell>[count];
}

// destructor
HTable::~HTable() {
    delete[] htable;
}

// inserts value at key (row)
void HTable::insert(int key, Cell cell) {
    htable[key].push_back(cell);
}

// prints entire hashtable (regions only)
void HTable::print() {
    for (int i = 0; i < count; i++) {
        for (Cell cell : htable[i]) {
            if (cell.population == 0) {
                cout << cell.region << " ";
            } else {
                cout << cell.population << " ";
            }
        }
    }
    cout << endl;
}

// getters and setters
list<Cell>& HTable::operator[](int index) {
    return htable[index];
}

int HTable::getCount() const {
    return count;
}

int HTable::getRowCount() const {
    return count;
}

int HTable::getColCount(int row) const {
    return htable[row].size();
}

Cell HTable::getCell(int row, int col) const {
    auto it = htable[row].begin();
    advance(it, col);
    return *it;
}

void HTable::updateCell(int row, int col, const Cell& updatedCell) {
    auto it = htable[row].begin();
    advance(it, col);
    *it = updatedCell;
}

void HTable::swap(HTable *other) {
    list<Cell> *temp = htable;
    htable = other->htable;
    other->htable = temp;
}

HTable* HTable::copy() const {
    HTable *newTable = new HTable(count);
    for (int i = 0; i < count; i++) {
        for (Cell cell : htable[i]) {
            newTable->insert(i, cell);
        }
    }
    return newTable;
}

list<Cell> HTable::getRow(int row_num) const {
    if (row_num >= 0 && row_num < count) {
        return htable[row_num];
    } else {
        throw std::out_of_range("Invalid row number");
    }
}