#include "powerline.h"

Powerline::Powerline(HTable *table) {
    this->table = table;
}

bool Powerline::isInBounds(int row, int col) {
    return row >= 0 && row < table->getRowCount() && col >= 0 && col < table->getColCount(row);
}

bool Powerline::dfs(int row, int col, bool **visited) {
    if (!isInBounds(row, col) || visited[row][col]) {
        return false;
    }

    visited[row][col] = true;
    Cell cell = table->getCell(row, col);

    if (cell.region == "P") {
        return true;
    }

    if (cell.region != "T" && cell.region != "#") {
        return false;
    }

    int dr[] = {-1, 0, 1, 0};
    int dc[] = {0, 1, 0, -1};

    for (int i = 0; i < 4; ++i) {
        int newRow = row + dr[i];
        int newCol = col + dc[i];

        if (dfs(newRow, newCol, visited)) {
            return true;
        }
    }

    return false;
}

bool Powerline::isConnectedToPowerPlant() {
    bool **visited = new bool *[table->getRowCount()];
    for (int i = 0; i < table->getRowCount(); ++i) {
        visited[i] = new bool[table->getColCount(i)];
        for (int j = 0; j < table->getColCount(i); ++j) {
            visited[i][j] = false;
        }
    }

    for (int row = 0; row < table->getRowCount(); ++row) {
        for (int col = 0; col < table->getColCount(row); ++col) {
            Cell cell = table->getCell(row, col);
            if (cell.region == "T" || cell.region == "#") {
                if (dfs(row, col, visited)) {
                    for (int i = 0; i < table->getRowCount(); ++i) {
                        delete[] visited[i];
                    }
                    delete[] visited;
                    return true;
                }
            }
        }
    }

    for (int i = 0; i < table->getRowCount(); ++i) {
        delete[] visited[i];
    }
    delete[] visited;
    return false;
}
