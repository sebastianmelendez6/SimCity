#include <iostream>
#include <vector>
#include "Residential.h"

using namespace std;

Residential::Residential(HTable *table)
{
    this->table = table;
}

vector<pair<int, int>> Residential::getNeighbors(int row, int col)
{
    vector<pair<int, int>> neighbors = {
        {row - 1, col - 1}, {row - 1, col}, {row - 1, col + 1}, {row, col - 1}, {row, col + 1}, {row + 1, col - 1}, {row + 1, col}, {row + 1, col + 1}};
    return neighbors;
}

bool Residential::isInBounds(int row, int col)
{
    return row >= 0 && row < table->getRowCount() && col >= 0 && col < table->getColCount(row);
}

int Residential::simulate()
{

    // declaring number of workers
    int workersIncreased = 0;

    // Creating a copy of the current state of the hashtable
    HTable *tableCopy = table->copy();

    // Iterating through each cell in the original hashtable
    for (int row = 0; row < table->getRowCount(); ++row)
    {
        for (int col = 0; col < table->getColCount(row); ++col)
        {
            Cell cell = table->getCell(row, col);

            // Check if the cell is a residential cell
            if (cell.region == "R")
            {
                // 4. Get the neighbors of the cell
                vector<pair<int, int>> neighbors = getNeighbors(row, col);

                // Count the number of residential neighbors with a certain population
                int count[5] = {0}; // counts for population 0 to 4
                bool hasPowerLine = false;
                for (pair<int, int> neighbor : neighbors)
                {
                    int r = neighbor.first;
                    int c = neighbor.second;

                    // Check if the neighbor is in bounds
                    if (isInBounds(r, c))
                    {
                        Cell nCell = table->getCell(r, c);

                        if (nCell.region == "T" || nCell.region == "#")
                        {
                            hasPowerLine = true;
                        }
                        else if (nCell.region == "R")
                        {
                            count[nCell.population]++;
                        }
                    }
                }

                // Applying the residential rules and update the copy of the hashtable
                if (cell.population < 4 && ((cell.population == 0 && (hasPowerLine || count[1] >= 1)) ||
                                            (cell.population == 1 && count[1] >= 2) ||
                                            (cell.population == 2 && count[2] >= 4) ||
                                            (cell.population == 3 && count[3] >= 6) ||
                                            (cell.population == 4 && count[4] >= 8)))
                {
<<<<<<< HEAD
                    Cell updatedCell = cell;
                    updatedCell.population++;
                    tableCopy->updateCell(row, col, updatedCell);

                    workersIncreased++; // Increase the workersIncreased counter
=======
                        Cell updatedCell = cell;
                        updatedCell.population++;
                        tableCopy->updateCell(row, col, updatedCell);

                        workersIncreased++; // Increase the workersIncreased counter
                    }
>>>>>>> 16dcdff295006253091dd15f1123f76c11059974
                }
            }
        }

        // Swap the original hashtable with the updated copy
        table->swap(tableCopy);

        // Clean up
        delete tableCopy;

        // return number of workers
        return workersIncreased;
    }
<<<<<<< HEAD

    // Swap the original hashtable with the updated copy
    table->swap(tableCopy);

    // Clean up
    delete tableCopy;

    // return number of workers
    return workersIncreased;
}
=======
>>>>>>> 16dcdff295006253091dd15f1123f76c11059974
