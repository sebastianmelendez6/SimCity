#ifndef POWERLINE_H
#define POWERLINE_H

#include "htable.h"

class Powerline {
public:
    Powerline(HTable *table);
    bool isConnectedToPowerPlant();

private:
    HTable *table;
    bool dfs(int row, int col, bool **visited);
    bool isInBounds(int row, int col);
};

#endif // POWERLINE_H
