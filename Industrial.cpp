//industrial.cpp start
#include <iostream>
#include <vector>
#include "Industrial.h"

using namespace std;

Industrial::Industrial(HTable *table) {
    this->table = table;
}

vector<pair<int, int>> Industrial::getNeighbors(int row, int col) {
    vector<pair<int, int>> neighbors = {
        {row - 1, col - 1}, {row - 1, col}, {row - 1, col + 1},
        {row, col - 1},                 {row, col + 1},
        {row + 1, col - 1}, {row + 1, col}, {row + 1, col + 1}
    };
    return neighbors;
}

bool Industrial::isInBounds(int row, int col) {
    return row >= 0 && row < table->getRowCount() && col >= 0 && col < table->getColCount(row);
}

int Industrial::simulate(int available_workers) {
    int workersAssigned = 0;

    // 1. Create a copy of the current state of the hashtable
    HTable *tableCopy = table->copy();

    // 2. Iterate through each cell in the original hashtable
    for (int row = 0; row < table->getRowCount(); ++row) {
        for (int col = 0; col < table->getColCount(row); ++col) {
            Cell cell = table->getCell(row, col);

            // 3. Check if the cell is an industrial cell
            if (cell.region == "I") {
                // 4. Get the neighbors of the cell
                vector<pair<int, int>> neighbors = getNeighbors(row, col);

                // 5. Count the number of industrial neighbors with a certain population
                int count[4] = {0}; // counts for population 0 to 3
                bool hasPowerLine = false;
                for (pair<int, int> neighbor : neighbors) {
                    int r = neighbor.first;
                    int c = neighbor.second;

                    // Check if the neighbor is in bounds
                    if (isInBounds(r, c)) {
                        Cell nCell = table->getCell(r, c);

                        if (nCell.region == "T" || nCell.region == "#") {
                            hasPowerLine = true;
                        } else if (nCell.region == "I") {
                            count[nCell.population]++;
                        }
                    }
                }

                // 6. Apply the industrial rules and update the copy of the hashtable
                if (cell.population < 3 && available_workers >= 2 && ((cell.population == 0 && (hasPowerLine || count[1] > 0)) || count[cell.population + 1] >= 2 * (cell.population + 1) - 1)) {
                    Cell updatedCell = cell;
                    updatedCell.population++;
                    tableCopy->updateCell(row, col, updatedCell);

                    workersAssigned += 2; // Assign 2 workers to the job
                    available_workers -= 2; // Reduce available workers
                    }
            }
        }
    }

    // 7. Update the original hashtable with the updated cells
    for (int row = 0; row < table->getRowCount(); ++row) {
        for (int col = 0; col < table->getColCount(row); ++col) {
            Cell cell = tableCopy->getCell(row, col);
            table->updateCell(row, col, cell);
        }
    }

    // Clean up
    delete tableCopy;

    // Return the number of workers assigned to jobs
    return workersAssigned;
}