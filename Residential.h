#ifndef RESIDENTIAL_H
#define RESIDENTIAL_H

#include <vector>
#include "htable.h"

using namespace std;

class Residential {
public:
    Residential(HTable *table);
    int simulate();

private:
    HTable*table;

    vector<pair<int, int>> getNeighbors(int row, int col);
    bool isInBounds(int row, int col);
};
#endif // residential.H