#include <iostream>
#include <list>
#include <string>
#include <fstream>
#include <vector>
#include "htable.h"
#include "Residential.h"
#include "Industrial.h"
#include "Commercial.h"
#include "powerline.h"
#include "Citypoppollution.h"

using namespace std;

int main()
{
    // input filename from user
    string file;
    cout << "Enter Configuration file name: ";
    cin >> file;

    // read config file
    fstream configFile;
    configFile.open(file, ios::in);
    string regionLt;
    string timeLimit;
    string refreshRate;
    if (configFile.is_open())
    {
        string line;
        while (!configFile.eof())
        {
            getline(configFile, line, ':');
            if (line == "Region Layout")
            {
                getline(configFile, regionLt);
            }
            else if (line == "Time Limit")
            {
                getline(configFile, timeLimit);
            }
            else if (line == "Refresh Rate")
            {
                getline(configFile, refreshRate);
            }
        }
        configFile.close();
    }
    else
    {
        cout << "error: UNABLE TO OPEN config FILE" << endl;
        return 1;
    }

    // read number of rows in region file
    fstream regionFile;
    regionFile.open(regionLt, ios::in);
    int numRows = 0;
    if (regionFile.is_open())
    {
        string l;
        while (!regionFile.eof())
        {
            getline(regionFile, l);
            numRows++;
        }
        regionFile.close();
    }
    else
    {
        cout << "error: UNABLE TO OPEN region FILE" << endl;
        return 2;
    }

    // create hashtable object
    HTable table(numRows);

    // read region file and add to hash table
    regionFile.open(regionLt, ios::in);
    if (regionFile.is_open())
    {
        string line;
        string val;
        int row = 0;
        while (!regionFile.eof())
        {
            getline(regionFile, val, ',');
            if (val.substr(0, 1) == "\n")
            {
                row++;
            }
            Cell newCell;
            newCell.region = val;
            newCell.population = 0;
            newCell.pollution = 0;
            table.insert(row, newCell);
        }
        regionFile.close();
    }
    else
    {
        cout << "error: UNABLE TO OPEN region FILE" << endl;
        return 3;
    }

    // print hash table (initial state)
    cout << "The initial state of the region is: " << endl;
    table.print();

    // powerline
    Powerline powerline(&table);
    if (powerline.isConnectedToPowerPlant())
    {
        cout << "The powerlines do meet the power plant!" << endl
             << endl;
    }
    else
    {
        cout << "The powerlines do NOT meet the power plant!" << endl
             << endl;
    }

    // simulation
    int time_step = 0;
    bool changed = true;
    int state = 0;
    int avail_workers = 0;
    int avail_goods = 0;

    // creating residential/industrial/commercial objects
    Residential residential(&table);
    Industrial industrial(&table);

    while (changed && time_step < stoi(timeLimit))
    {
        int commercial_growth = 0, residential_growth = 0, industrial_growth = 0;
        changed = false;

        /// Call the residential, commercial and industrial functions
        commercial_growth = simulateCommercial(avail_workers, avail_goods, &table);
        avail_workers -= commercial_growth;
        avail_goods -= commercial_growth;
        industrial_growth = industrial.simulate(avail_workers);
        avail_workers -= industrial_growth;
        avail_goods += industrial_growth / 2;
        residential_growth = residential.simulate();
        avail_workers += residential_growth;

        // printing each state based on frequency
        if ((time_step++ % stoi(refreshRate)) == 0)
        {
            cout << "state: " << time_step << endl;
            table.print();
            cout << "The number of available workers is: " << avail_workers << endl;
            cout << "The number of available goods is: " << avail_goods << endl
                 << endl;
            cout << "residential growth: " << residential_growth << " commercial growth: " << commercial_growth << " industrial growth: " << industrial_growth << endl;
        }

        if (residential_growth > 0 || industrial_growth > 0 || commercial_growth > 0)
        {
            changed = true;
        }
        else
        {
            cout << "No changes in the last time step" << endl;
        }
    }

    // print table (final state)
    cout << "Final state is: " << endl;
    table.print();

    int total_population = 0;
    int residential_population = 0;
    int industrial_population = 0;
    int commercial_population = 0;

    for (int i = 0; i < numRows; ++i)
    {
        list<Cell> row_cells = table.getRow(i);
        for (const Cell &cell : row_cells)
        {
            total_population += cell.population;
            if (cell.region == "R")
            {
                residential_population += cell.population;
            }
            else if (cell.region == "I")
            {
                industrial_population += cell.population;
            }
            else if (cell.region == "C")
            {
                commercial_population += cell.population;
            }
        }
    }

    cout << "Total Population: " << total_population << endl;
    cout << "Residential Population: " << residential_population << endl;
    cout << "Industrial Population: " << industrial_population << endl;
    cout << "Commercial Population: " << commercial_population << endl;

     //  add pollution to industrial cells
for (int row = 0; row < table.getRowCount(); ++row)
{
    for (int col = 0; col < table.getColCount(row); ++col)
    {
        Cell cell = table.getCell(row, col);

        if (cell.region == "I")
        {
            // calculate pollution amount based on population and goods
            int pollutionAmount = (cell.population + avail_workers) / 10;

            // add pollution to cell
            void pollute(HTable *table, int row,int col, int pollutionAmount);
        }
    }
}
   void DisplayPollutionTable(HTable *table);

    return 0;
}
