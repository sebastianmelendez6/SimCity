#include <iostream>
#include <list>
#include <string>
#include <vector>
#include "Citypoppollution.h"


using namespace std;
      

        // pollution function
void pollute(HTable *table, int row, int col, int pollutionAmount)
{
    Cell cell = table->getCell(row, col);

    // add pollution to cell
    cell.pollution += pollutionAmount;

    // determine adjacent cells
    list<pair<int, int>> adjCells;
    adjCells.push_back(make_pair(row - 1, col)); // top
    adjCells.push_back(make_pair(row + 1, col)); // bottom
    adjCells.push_back(make_pair(row, col - 1)); // left
    adjCells.push_back(make_pair(row, col + 1)); // right

    // spread pollution to adjacent cells
    for (auto iter = adjCells.begin(); iter != adjCells.end(); ++iter)
    {
        int adjRow = iter->first;
        int adjCol = iter->second;

        if (adjRow >= 0 && adjRow < table->getRowCount() &&
            adjCol >= 0 && adjCol < table->getColCount(adjRow))
        {
            Cell adjCell = table->getCell(adjRow, adjCol);

            // calculate pollution spread based on distance
            int dist = abs(row - adjRow) + abs(col - adjCol);
            int adjPollution = pollutionAmount / (dist + 1);

            // add pollution to adjacent cell
            adjCell.pollution += adjPollution;

            // update adjacent cell in table
            table->updateCell(adjRow, adjCol, adjCell);
        }
    }

    // update cell in table
    table->updateCell(row, col, cell);
}

void DisplayPollutionTable(HTable *table)
{
    for (int i = 0; i < table->getRowCount(); i++)
    {
        for (int j = 0; j < table->getColCount(i); j++)
        {
            Cell currCell = table->getCell(i, j);
            cout << "Row " << i << ", Col " << j << ": Pollution " << currCell.pollution << endl;
        }
    }
}

