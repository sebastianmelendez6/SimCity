#ifndef HTABLE_H
#define HTABLE_H

#include <iostream>
#include <list>
#include <string>

using namespace std;

struct Cell {
    string region;
    int population;
    int pollution;
};

class HTable {
private:
    list<Cell> *htable;
    int count;

public:
    // constructor
    HTable(int x);

    // destructor
    ~HTable();

    // inserts value at key (row)
    void insert(int key, Cell cell);

    // prints entire hashtable (regions only)
    void print();

    // getters and setters
    list<Cell>& operator[](int index);
    int getCount() const;

    // functions
    int getRowCount() const;
    int getColCount(int row) const;
    Cell getCell(int row, int col) const;
    void updateCell(int row, int col, const Cell& updatedCell);
    void swap(HTable *other);
    HTable* copy() const;
    list<Cell> getRow(int row_num) const;
};

#endif // HTABLE_H