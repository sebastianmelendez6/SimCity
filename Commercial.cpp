#include <algorithm>
#include "Commercial.h"

using namespace std;

int simulateCommercial(int avail_workers, int avail_goods, HTable* table)
{
	vector<cellArray> adjList;
	array<Cell, 8> adjacents;
	Cell cell;
	Cell emptyCell = { "", 0, 0 };
	int avail_commercial = min(avail_workers, avail_goods);
	int commercial_growth = 0;
	int table_rows = table->getRowCount();

	// Populate adjacency list with all commercial cells and their adjacent cells
	// if an adjacent cell is out of bounds, place empty cell in adjList
	for (int row = 0; row < table_rows; ++row) {
		int table_cols = table->getColCount(row);
		for (int col = 0; col < table_cols; ++col) {
			cell = table->getCell(row, col);
			if (cell.region == "C") {
				if (row - 1 >= 0 && col - 1 >= 0) // up and left
					adjacents[0] = table->getCell(row - 1, col - 1);
				else
					adjacents[0] = emptyCell;
				if (row - 1 >= 0) // up
					adjacents[1] = table->getCell(row - 1, col);
				else
					adjacents[1] = emptyCell;
				if (row - 1 >= 0 && col + 1 <= table_cols) // up and right
					adjacents[2] = table->getCell(row - 1, col + 1);
				else
					adjacents[2] = emptyCell;
				if (col - 1 >= 0) // left
					adjacents[3] = table->getCell(row, col - 1);
				else
					adjacents[3] = emptyCell;
				if (col + 1 < table_cols) // right
					adjacents[4] = table->getCell(row, col + 1);
				else
					adjacents[4] = emptyCell;
				if (row + 1 <= table_rows && col - 1 >= 0) // down and left
					adjacents[5] = table->getCell(row + 1, col - 1);
				else
					adjacents[5] = emptyCell;
				if (row + 1 <= table_rows) // down
					adjacents[6] = table->getCell(row + 1, col);
				else
					adjacents[6] = emptyCell;
				if (row + 1 <= table_rows && col + 1 <= table_cols) // down and right
					adjacents[7] = table->getCell(row + 1, col + 1);
				else
					adjacents[7] = emptyCell;
				cellArray temp(row, col, cell, adjacents);
				adjList.push_back(temp);
			}
		}
	}
	// sort adjacency list by population and then by adjacent population, then by row and col
	sort(adjList.begin(), adjList.end(), [](const cellArray& a, const cellArray& b)
		{
			if (a.cell.population == b.cell.population) {
				int adjPopA = 0, adjPopB = 0;
				for (int i = 1; i < 5; ++i) {
					adjPopA += a.adjacentCells[i].population;
					adjPopB += b.adjacentCells[i].population;
				}
				if (adjPopA == adjPopB) {
					if (a.row == b.row) {
						return a.col < b.col;
					}
					else { return a.row < b.row; }
				}
				else { return adjPopA > adjPopB; }
			}
			else { return a.cell.population > b.cell.population; }
		});

	for (auto& list : adjList) {
		bool adjPower = false;
		int adjPopulatedCells = 0;
		for (auto& adjCell : list.adjacentCells) {
			if (adjCell.region == "T" || adjCell.region == "#")
				adjPower = true;
			if (adjCell.population > 0)
				adjPopulatedCells++;
		}
		cell = list.cell;
		if (avail_commercial > 0 &&
			((cell.population == 1 && adjPopulatedCells >= 2) ||
			(cell.population == 0 && (adjPopulatedCells >= 1 || 
										adjPower)))) {
			cell.population++;
			avail_commercial--;
			commercial_growth++;
			table->updateCell(list.row, list.col, cell);
		}
	}
	return commercial_growth;
}
