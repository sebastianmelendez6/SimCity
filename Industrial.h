#ifndef INDUSTRIAL_H
#define INDUSTRIAL_H

#include "htable.h"
#include <vector>
//industrial.h
class Industrial {
public:
    Industrial(HTable *table);
    int simulate(int available_workers);

private:
    HTable *table;
    std::vector<std::pair<int, int>> getNeighbors(int row, int col);
    bool isInBounds(int row, int col);
};

#endif // INDUSTRIAL_H