Group 5:
Areeb Ashraf
Sebastian Melendez
Jayendra Minakshisundar
Shailesh Potula
Chase Richardson
Emmanuel Rodriguez Padron

Instructions to compile and execute: -
    To compile: g++ main.cpp htable.cpp Residential.cpp Industrial.cpp Commercial.cpp powerline.cpp Citypoppollution.cpp  -o main
    To execute: ./main

Bonus implementation: Traversing power lines to guarantee that they reach a power plant.
